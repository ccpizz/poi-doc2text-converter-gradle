# Word to plain text converter #

Run the converter with gradle:

------------

        gradle run

------------

### Generate a binary distribution with shell script runner:

---------- 

        gradle installDist


---------------
* _The script will be created under `<PROJECT_DIR>/build/install/wordconv/bin/wordconv`_
   
### Generate a zipped binary distribution:

------------------
  
        gradle distZip


-----------------
* _The zip file will created under `<PROJECT_DIR>/build/distributions`_